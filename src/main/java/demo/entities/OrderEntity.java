package demo.entities;


import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity(name = "Order")  
@Table(name = "TB_ECOM_ORDER")
public class OrderEntity  {   
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private Long id;

	
	@Column(name="code", nullable=false, unique=true)  
	private String code;


	@Column(name="date", nullable=false)
	@JsonFormat(pattern="dd/MM/yyyy")
	private  LocalDate date;

	@Column(name="total_amount", nullable=false)  
	@JsonProperty("total_amount")
	private BigDecimal totalAmount;
	
	
	@Column(name="status", nullable=true)  
	private String status;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private CustomerEntity customer;
	
	
	public String getStatus() {
		return status;
	}

	

	public void setStatus(String status) {
		this.status = status;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}



	public CustomerEntity getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}



	public BigDecimal getTotalAmount() {
		return totalAmount;
	}


	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}



}